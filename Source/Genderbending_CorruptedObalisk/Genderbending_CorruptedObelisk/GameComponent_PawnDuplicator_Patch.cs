﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Verse;

namespace Genderbending_CorruptedObelisk
{
    [HarmonyPatch(typeof(GameComponent_PawnDuplicator), nameof(GameComponent_PawnDuplicator.Duplicate))]
    public static class GameComponent_PawnDuplicator_Patch
    {
        static float TriggerChance => Genderbending_CorruptedObelisk_Mod.settings.TriggerChance;
        static float MaleTriggerChance => Genderbending_CorruptedObelisk_Mod.settings.MaleTriggerChance;
        static float FemaleTriggerChance => Genderbending_CorruptedObelisk_Mod.settings.FemaleTriggerChance;
        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> changegender(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codeInstructions = instructions.ToList();
            // anchoring on the storing of the num local field, using the operand as a local builder
            //int anchorIndex = codeInstructions.FindIndex(ci => ci.opcode == OpCodes.Stloc_S && ci.operand is LocalBuilder builder && builder.LocalIndex == 7);

            FieldInfo fieldInfo = AccessTools.Field(typeof(Pawn), nameof(Pawn.gender));
            int anchorIndex = codeInstructions.FindIndex(ci => ci.LoadsField(fieldInfo));
            if (anchorIndex == -1)
            {
                Log.Error($"Anchor could not be found, aborting patch");
                return codeInstructions.AsEnumerable();
            }
            //anchorIndex -= 1;
            const int instructionsToRemove = 1;
            CodeInstruction instructionWithLabels = codeInstructions.ElementAt(anchorIndex);
            codeInstructions.RemoveRange(anchorIndex, instructionsToRemove);
            codeInstructions.InsertRange(anchorIndex, InjectedInstructions());
            codeInstructions.ElementAt(anchorIndex).MoveLabelsFrom(instructionWithLabels);

            return codeInstructions.AsEnumerable();
        }
        private static IEnumerable<CodeInstruction> InjectedInstructions()
        {
            MethodInfo changeGenderMethod = AccessTools.Method(typeof(GameComponent_PawnDuplicator_Patch), nameof(GameComponent_PawnDuplicator_Patch.ChangeGenderHelper));

            //yield return new CodeInstruction(OpCodes.Ldfld);
            yield return new CodeInstruction(OpCodes.Call, changeGenderMethod);
        }

        public static Gender ChangeGenderHelper(Pawn pawn)
        {
            if (!Rand.Chance(TriggerChance) && !Genderbending_CorruptedObelisk_Mod.settings.EnableGenderSpecificChances)
            {
                return pawn.gender;
            }
            if (Genderbending_CorruptedObelisk_Mod.settings.EnableGenderSpecificChances)
            {
                if (!Rand.Chance(MaleTriggerChance) && pawn.gender == Gender.Male)
                {
                    return pawn.gender;
                }
                if (!Rand.Chance(FemaleTriggerChance) && pawn.gender == Gender.Female)
                {
                    return pawn.gender;
                }
            }
            if (pawn.genes.GenesListForReading.Any(gene => gene.def.HasModExtension<GeneDefExtensions_BlockGenderbend>()))
            {
                return pawn.gender;
            }
            switch (pawn.gender)
            {
                case Gender.Male: return Gender.Female;
                case Gender.Female: return Gender.Male;
                case Gender.None: return Gender.None;
                default: return pawn.gender;// if a mod adds a gender
            }
        }
    }

    [HarmonyPatch(typeof(GameComponent_PawnDuplicator), nameof(GameComponent_PawnDuplicator.Duplicate))]
    public static class GameComponent_PawnDuplicatorRelationship_patch
    {
        [HarmonyPostfix]
        public static void AddBloodRelations(Pawn __result, Pawn pawn)
        {
            Pawn duplicatePawn = __result;
            if (!Genderbending_CorruptedObelisk_Mod.settings.EnableDuplicateRelation)
            {
                return;
            }
            PawnRelationDef relationToGive = GetDuplicateRelationFor(pawn);

            List<DirectPawnRelation> pawnRelations = pawn.relations.DirectRelations;
            foreach (DirectPawnRelation directRelation in pawnRelations)
            {
                if (!Common.AllDuplicateRelations.Contains(directRelation.def))
                {
                    continue;
                }
                Pawn otherDuplicatePawn = directRelation.otherPawn;
                duplicatePawn.relations.AddDirectRelation(relationToGive, otherDuplicatePawn);
                otherDuplicatePawn.relations.AddDirectRelation(relationToGive, duplicatePawn);
            }
            duplicatePawn.relations.AddDirectRelation(relationToGive, pawn);
            pawn.relations.AddDirectRelation(relationToGive, duplicatePawn);
        }
        public static PawnRelationDef GetDuplicateRelationFor(Pawn pawn)
        {
            //ToDo: override with DefModExtensions
            BackstoryDef childStory = pawn.story.Childhood;
            BackstoryDef adultStory = pawn.story.Adulthood;
            if (adultStory != null)
            {
                BackstoryDefExtensions_ForcedDuplicateRelation forcedRelationExtension = adultStory.GetModExtension<BackstoryDefExtensions_ForcedDuplicateRelation>();
                if (forcedRelationExtension != null)
                {
                    return forcedRelationExtension.forcedDuplicateRelation;
                }
            }
            if (childStory != null)
            {
                BackstoryDefExtensions_ForcedDuplicateRelation forcedRelationExtension = childStory.GetModExtension<BackstoryDefExtensions_ForcedDuplicateRelation>();
                if (forcedRelationExtension != null)
                {
                    return forcedRelationExtension.forcedDuplicateRelation;
                }
            }

            string pawnStories = "";
            if (childStory != null)
            {
                pawnStories += childStory.defName;
            }
            if (adultStory != null)
            {
                pawnStories += adultStory.defName;
            }
            Random RNG = new Random(pawnStories.GetHashCode());
            int index = RNG.Next(Common.AllDuplicateRelations.Count);

            PawnRelationDef chosenRelation = Common.AllDuplicateRelations[index];
            return chosenRelation;
        }
    }

    [HarmonyPatch(typeof(GameComponent_PawnDuplicator), "CopyApperance")]
    public static class GameComponent_PawnDuplicatorApperance_Patch
    {
        // change bodyType & headType
        [HarmonyPostfix]
        public static void ChangeApperance(Pawn pawn, Pawn newPawn)
        {
            if (newPawn.gender == pawn.gender)
            {
                return;
            }
            //newPawn.story.TryGetRandomHeadFromSet();
            newPawn.story.TryGetRandomHeadFromSet(from x in DefDatabase<HeadTypeDef>.AllDefs
                                                  where x.randomChosen
                                                  select x);

            newPawn.story.bodyType = PawnGenerator.GetBodyTypeFor(newPawn); // without HAR this gives the wrong bodyType // actually no Vanilla just lists <bodyTypeFemale>Male</bodyTypeFemale> often

            newPawn.story.hairDef = PawnStyleItemChooser.RandomHairFor(newPawn);
        }
    }

    [HarmonyPatch(typeof(GameComponent_PawnDuplicator), "CopyStyle")]
    public static class GameComponent_PawnDuplicatorStyle_Patch
    {
        [HarmonyPostfix]
        public static void ChangeStyle(Pawn pawn, Pawn newPawn)
        {
            if (newPawn.gender == pawn.gender)
            {
                return;
            }
            if (newPawn.gender == Gender.Female && !newPawn.genes.HasActiveGene(GCO_DefOfs.Beard_Always) && newPawn.style.beardDef != BeardDefOf.NoBeard)
            {
                newPawn.style.beardDef = BeardDefOf.NoBeard;
            }
        }
    }

    [HarmonyPatch(typeof(GameComponent_PawnDuplicator), "CopyHediffs")]
    public static class GameComponent_PawnDuplicatorHediff_Patch
    {
        [HarmonyPostfix]
        public static void ChangeHediffs(Pawn pawn, Pawn newPawn)
        {
            if (newPawn.gender == pawn.gender)
            {
                return;
            }
            List<Hediff> hediffs = newPawn.health.hediffSet.hediffs.ToList();
            foreach (Hediff hediff in hediffs)
            {
                if (hediff.def.HasModExtension<HediffExtension_RemoveOnGenderChange>())
                {
                    newPawn.health.RemoveHediff(hediff);
                }
            }
        }
    }
}