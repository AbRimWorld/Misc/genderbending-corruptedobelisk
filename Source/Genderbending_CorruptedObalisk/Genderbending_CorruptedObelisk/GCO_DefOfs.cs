﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Genderbending_CorruptedObelisk
{
	[DefOf]
	public static class GCO_DefOfs
	{
		public static PawnRelationDef GCO_DuplicateGreaterPositive;
		public static PawnRelationDef GCO_DuplicatePositive;
		public static PawnRelationDef GCO_Duplicate;
		public static PawnRelationDef GCO_DuplicateNegative;
		public static PawnRelationDef GCO_DuplicateGreaterNegative;

		[MayRequireBiotech]
		public static GeneDef Beard_Always;

		static GCO_DefOfs()
		{
			DefOfHelper.EnsureInitializedInCtor(typeof(GCO_DefOfs));
		}
	}
}
