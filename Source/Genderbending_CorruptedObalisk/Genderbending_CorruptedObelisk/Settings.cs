﻿using UnityEngine;
using Verse;

namespace Genderbending_CorruptedObelisk
{
    public class Settings : ModSettings
    {
        const float minChance = 0f;
        const float maxChance = 1f;

        static readonly FloatRange sliderRange = new FloatRange(minChance, maxChance);

        public float TriggerChance = 0.2f;
        public float MaleTriggerChance = 0.2f;
        public float FemaleTriggerChance = 0.2f;

        public bool EnableGenderSpecificChances = false;

        public bool EnableDuplicateRelation = false;
        public bool EnableDuplicateFamilyBloodRelations = false;

        public void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);

            listingStandard.CheckboxLabeled("GCO_GenderSpecificChances".Translate(), ref EnableGenderSpecificChances, "GCO_GenderSpecificChances_Tip".Translate());

            if (!EnableGenderSpecificChances)
            {
                Rect triggerChanceSliderRect = listingStandard.GetRect(Text.LineHeight * 2);
                string chanceLabel = TriggerChance.ToStringByStyle(ToStringStyle.PercentOne);
                Widgets.HorizontalSlider(triggerChanceSliderRect, ref TriggerChance, sliderRange, $"GCO_TriggerChanceSlider".Translate(chanceLabel), 0);
            }

            if (EnableGenderSpecificChances)
            {
                Rect maleTriggerChanceSliderRect = listingStandard.GetRect(Text.LineHeight * 2);
                string maleChanceLabel = MaleTriggerChance.ToStringByStyle(ToStringStyle.PercentOne);
                Widgets.HorizontalSlider(maleTriggerChanceSliderRect, ref MaleTriggerChance, sliderRange, $"GCO_TriggerChanceSlider_Male".Translate(maleChanceLabel), 0);

                Rect femaleTriggerChanceSliderRect = listingStandard.GetRect(Text.LineHeight * 2);
                string femaleChanceLabel = FemaleTriggerChance.ToStringByStyle(ToStringStyle.PercentOne);
                Widgets.HorizontalSlider(femaleTriggerChanceSliderRect, ref FemaleTriggerChance, sliderRange, $"GCO_TriggerChanceSlider_Female".Translate(femaleChanceLabel), 0);
            }

            listingStandard.CheckboxLabeled("GCO_DuplicateRelation".Translate(), ref EnableDuplicateRelation, "GCO_DuplicateRelation_Tip".Translate());
            //listingStandard.CheckboxLabeled("GCO_DuplicateFamilyBloodRelations".Translate(), ref EnableDuplicateFamilyBloodRelations, "GCO_DuplicateFamilyBloodRelations_Tip".Translate());
            listingStandard.End();
        }
        public override void ExposeData()
        {
            Scribe_Values.Look(ref TriggerChance, "GCO_TriggerChance", defaultValue: 0.2f);
            Scribe_Values.Look(ref MaleTriggerChance, "GCO_MaleTriggerChance", defaultValue: 0.2f);
            Scribe_Values.Look(ref FemaleTriggerChance, "GCO_FemaleTriggerChance", defaultValue: 0.2f);

            Scribe_Values.Look(ref EnableGenderSpecificChances, "GCO_EnableGenderSpecificChances", defaultValue: false);
            Scribe_Values.Look(ref EnableDuplicateRelation, "GCO_EnableDuplicateRelation", defaultValue: false);
            Scribe_Values.Look(ref EnableDuplicateFamilyBloodRelations, "GCO_EnableDuplicatingBloodRelations", defaultValue: false);
            base.ExposeData();
        }
    }

    public class Genderbending_CorruptedObelisk_Mod : Mod
    {
        public static Settings settings;
        public Genderbending_CorruptedObelisk_Mod(ModContentPack content) : base(content)
        {
            settings = GetSettings<Settings>();
        }
        public override void DoSettingsWindowContents(Rect inRect)
        {
            settings.DoSettingsWindowContents(inRect);
        }
        public override string SettingsCategory()
        {
            return "Genderbending_CorruptedObelisk".Translate();
        }
    }
}