﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genderbending_CorruptedObelisk
{
	public class Common
	{
		public static List<PawnRelationDef> AllDuplicateRelations = new List<PawnRelationDef>()
			{
				GCO_DefOfs.GCO_DuplicateGreaterPositive,
				GCO_DefOfs.GCO_DuplicatePositive,
				GCO_DefOfs.GCO_Duplicate,
				GCO_DefOfs.GCO_DuplicateNegative,
				GCO_DefOfs.GCO_DuplicateGreaterNegative,
			};

	}
}
