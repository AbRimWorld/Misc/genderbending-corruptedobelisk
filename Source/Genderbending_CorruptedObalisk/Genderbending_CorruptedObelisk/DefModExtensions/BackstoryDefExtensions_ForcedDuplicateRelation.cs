﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Genderbending_CorruptedObelisk
{
	public class BackstoryDefExtensions_ForcedDuplicateRelation : DefModExtension
	{
		public PawnRelationDef forcedDuplicateRelation = null;

		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string error in base.ConfigErrors())
			{
				yield return error;
			}
		}
	}
}