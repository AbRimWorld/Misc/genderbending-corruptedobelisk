﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using AlienRace;

namespace Genderbending_CorruptedObelisk_HAR
{
	[HarmonyPatch(typeof(Genderbending_CorruptedObelisk.GameComponent_PawnDuplicator_Patch), nameof(Genderbending_CorruptedObelisk.GameComponent_PawnDuplicator_Patch.ChangeGenderHelper))]
	public class GameComponent_PawnDuplicator_Patch
	{
		[HarmonyPrefix]
		public static bool ShouldSkipGenderPatch(ref Gender __result, Pawn pawn)
		{
			try
			{
				if (!(pawn.def is ThingDef_AlienRace alienRaceDef))
				{
					return true;
				}
				if (alienRaceDef.alienRace.generalSettings.maleGenderProbability < 0.05f || alienRaceDef.alienRace.generalSettings.maleGenderProbability > 0.95f)
				{
					__result = pawn.gender;
					return false;
				}
				return true;
			}
			catch
			{
				return true;
			}
		}
	}

	[HarmonyPatch(typeof(GameComponent_PawnDuplicator), "CopyApperance")]
	public static class GameComponent_PawnDuplicatorApperance_Patch
	{
		[HarmonyPostfix]
		public static void ChangeApperance(Pawn pawn, Pawn newPawn)
		{
			if (newPawn.gender == pawn.gender)
			{
				return;
			}
			HarmonyPatches.HeadTypeFilter(DefDatabase<HeadTypeDef>.AllDefs.Where((HeadTypeDef x) => x.randomChosen), newPawn);
		}
	}
}