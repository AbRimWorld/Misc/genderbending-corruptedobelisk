﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Genderbending_CorruptedObelisk_HAR
{
	[StaticConstructorOnStartup]
	public static class Startup
	{
		static Startup()
		{
			Harmony harmony = new Harmony("Genderbending_CorruptedObelisk_HAR");
			Harmony.DEBUG = false;//never release a build with this set to true.
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
	}
}
